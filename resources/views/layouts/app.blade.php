<!DOCTYPE html>
<html>

<head>
    <!-- Basic -->
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <!-- Mobile Metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <!-- Site Metas -->
    <meta name="keywords" content="" />
    <meta name="description" content="" />
    <meta name="author" content="" />

    <title>Joson</title>

    <!-- bootstrap core css -->
    <link rel="stylesheet" type="text/css" href="{{asset('./style/css/bootstrap.css')}}" />
    <!-- fonts style -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,700|Poppins:400,600,700&display=swap"
        rel="stylesheet" />
    <!-- Custom styles for this template -->
    <link href="{{asset('./style/css/style.css')}}" rel="stylesheet" />
    <!-- responsive style -->
    <link href="{{asset('./style/css/responsive.css')}}" rel="stylesheet" />
    <link rel="stylesheet" href="https://cdn.datatables.net/1.11.3/css/dataTables.bootstrap4.min.css">
</head>

<body class="sub_page">
    <div class="hero_area">
        <!-- header section strats -->
        <header class="header_section">
            <div class="container-fluid">
                <nav class="navbar navbar-expand-lg custom_nav-container ">
                    <a class="navbar-brand" href="{{url('/')}}">
                        <img src="{{asset('./img/logo.webp')}}" alt="logo" height="70" width="70">
                    </a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse"
                        data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                        aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>

                    <div class="collapse navbar-collapse ml-auto" id="navbarSupportedContent">
                        <ul class="navbar-nav  ml-auto">
                            <li class="nav-item {{ Request::is('/') ? 'active' : '' }}">
                                <a class="nav-link" href="{{ url('/') }}">Beranda <span class="sr-only">(current)</span></a>
                            </li>
                            @auth
                            <li class="nav-item {{ Request::is('profil') ? 'active' : '' }}">
                                <a class="nav-link" href="{{ url('/profil') }}">Profil</a>
                            </li>
                            @endauth
                            @guest
                            <li class="nav-item {{ Request::is('register') ? 'active' : '' }}">
                                <a class="nav-link" href="{{ route('register') }}">Register</a>
                            </li>
                            <li class="nav-item {{ Request::is('login') ? 'active' : '' }}">
                                <a class="nav-link" href="{{ route('login') }}">Login</a>
                            </li>
                            @endguest
                            @auth
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">Logout</a>
                            </li>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                            @endauth
                        </ul>
                    </div>
                </nav>
            </div>
        </header>
        <!-- end header section -->
    </div>

    <!-- contact section -->

    <section class="contact_section">
        @yield('content')
    </section>

    <!-- end contact section -->

    <!-- info section -->
    <section class="info_section layout_padding">
        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    <div class="info_menu">
                        <h5>
                            QUICK LINKS
                        </h5>
                        <ul class="navbar-nav  ">
                            <li class="nav-item active">
                                <a class="nav-link" href="https://www.facebook.com" target="_blank"> Facebook <span class="sr-only">(current)</span></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="https://www.instagram.com" target="_blank"> Instagram </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="https://www.twitter.com" target="_blank"> Twitter </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="https://www.youtube.com" target="_blank"> Youtube </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="https://www.tiktok.com" target="_blank"> Tiktok </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="https://web.telegram.org" target="_blank"> Telegram </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="info_course">
                        <h5>
                            Catalog Programmer
                        </h5>
                        <p>
                            Membantu anda para pencari kerja untuk mencari para programmer handal dengan keahlian yang spesifik
                        </p>
                    </div>
                </div>

                <div class="col-md-5 offset-md-1">
                    <div class="info_news">
                        <h5>
                            FOR ANY QUERY, PLEASE WRITE TO US
                        </h5>
                        <div class="info_contact">
                            <a href="">
                                Location : Yogyakarta
                            </a>
                            <a href="">
                                demo@gmail.com
                            </a>
                            <a href="">
                                Call : +01 1234567890
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- end info section -->

    <!-- footer section -->
    <footer class="container-fluid footer_section">
        <p>
            &copy; 2021 All Rights Reserved By
            <a href="https://html.design/">Creator</a>
        </p>
    </footer>
    <footer class="container-fluid footer_section">

        <p>
            Distributed By
            <a href="https://themewagon.com/">Themewagon</a>
        </p>

    </footer>
    <!-- footer section -->

    <script type="text/javascript" src="{{asset('./style/js/jquery-3.4.1.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('./style/js/bootstrap.js')}}"></script>
    @stack('scripts')
    @include('sweetalert::alert')
</body>

</html>
