@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-6">
            <div class="detail-box">
                <div class="heading_container">
                    <h3>
                        <a href="{{url('/profil')}}">Profil</a>
                        <span>/Update Skill</span>
                    </h3>
                    <p>
                        Anda dapat mengubah skill anda disini
                    </p>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="contact-form">
                <h5>
                    Update Skill
                </h5>
                <form action="/userskill/{{$userskill->id}}" method="POST">
                    @csrf
                    @method('put')
                    <div class="form-group">
                        <label for="exampleFormControlSelect1" class="text-white">Skill</label>
                        <select class="form-control" id="skill_id" name="skill_id">
                            <option value="{{$userskill->id}}" selected="selected" hidden="hidden">
                                {{$userskill->skill->skill}}</option>
                            @foreach($skill as $value)
                            <option value="{{$value->id}}">{{$value->skill}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="d-flex justify-content-center">
                        <button type="submit" class="btn_on-hover tombol">
                            Update
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection