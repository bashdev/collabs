@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-6">
            <div class="detail-box">
                <h3>
                    Masuk untuk bergabung dengan kami
                </h3>
                <p>
                    Buat akun gratis anda dan temukan Programmer yang anda cari dan buat komunitas
                </p>
                <a href="{{url('/register')}}">
                    Daftar Sekarang
                </a>
            </div>
        </div>
        <div class="col-md-6 p-4">
            <div class="login_form card p-4 bg-secondary">
                <h5 class="mx-auto text-light font-weight-bold mb-4">
                    LOGIN
                </h5>
                <form method="POST" action="{{ route('login') }}">
                    @csrf
                    <div class="mb-4">
                        <input id="email" type="email" class="form-control @error('email') is-invalid @enderror"
                            name="email" value="{{ old('email') }}" required autocomplete="email" autofocus placeholder="Email" />
                        @error('email')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <div class="mb-4">
                        <input id="password" type="password"
                            class="form-control @error('password') is-invalid @enderror" name="password" required
                            autocomplete="current-password" placeholder="Password"/>
                        @error('password')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <div class="d-flex justify-content-center">
                    <button type="submit" class="btn btn-success">Login</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection