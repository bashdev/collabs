@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-6">
            <div class="detail-box">
                <div class="heading_container">
                    <h3>
                        Daftar Sekarang
                    </h3>
                    <p>
                        Catalog programmer didedikasikan untuk kemajuan Bangsa
                    </p>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="contact-form">
                <h5>
                    Register
                </h5>
                <form method="POST" action="{{ route('register') }}" enctype="multipart/form-data">
                    @csrf
                    <div>
                        <input id="name" type="text" class=" @error('name') is-invalid @enderror"
                            name="name" value="{{ old('name') }}" required autocomplete="name" autofocus placeholder="Nama"> 

                        @error('name')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <div>
                        <input id="email" type="email" class=" @error('email') is-invalid @enderror"
                            name="email" value="{{ old('email') }}" required autocomplete="email" placeholder="Email">

                        @error('email')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <div>
                        <input id="password" type="password"
                            class=" @error('password') is-invalid @enderror" name="password" required
                            autocomplete="new-password" placeholder="Password">

                        @error('password')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <div>
                        <input id="password-confirm" type="password" name="password_confirmation"
                            required autocomplete="new-password" placeholder="Ulangi Password">
                    </div>
                    <div>
                        <input id="bio" type="text" class=" @error('bio') is-invalid @enderror" name="bio"
                            value="{{ old('bio') }}" required autocomplete="bio" placeholder="Bio">

                        @error('bio')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <div>
                        <input id="alamat" type="text" class=" @error('alamat') is-invalid @enderror"
                            name="alamat" value="{{ old('alamat') }}" required autocomplete="alamat" placeholder="Alamat">

                        @error('alamat')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <div>
                        <input id="nohp" type="text" class=" @error('nohp') is-invalid @enderror"
                            name="nohp" value="{{ old('nohp') }}" required autocomplete="nohp" placeholder="Telepon">

                        @error('nohp')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <div class="d-flex">
                        <label for="fotoprofil" class="my-auto mr-3 text-light">Avatar</label>
                        <input id="fotoprofil" type="file"
                            class="form-control @error('fotoprofil') is-invalid @enderror" name="fotoprofil"
                            value="{{ old('fotoprofil') }}" required autocomplete="fotoprofil" autofocus>

                        @error('fotoprofil')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <div class="d-flex justify-content-center mt-3">
                        <button type="submit" class="btn btn-success">
                            Register
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection