@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-6">
            <div class="detail-box">
                <div class="heading_container">
                    <h3>
                        <a href="{{url('/profil')}}">Profil</a>
                        <span>/Update Sosmed</span>
                    </h3>
                    <p>
                        Anda dapat mengubah sosmed anda disini
                    </p>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="contact-form">
                <h5>
                    Update Sosial Media
                </h5>
                <form action="/sosmed/{{$sosmed->id}}" method="POST">
                    @csrf
                    @method('put')
                    <div>
                        <input type="text" placeholder="Sosial Media" id="sosmed" name="sosmed"
                            value="{{$sosmed->sosmed}}" />
                        @error('sosmed')
                        <div class="alert alert-danger">
                            {{message}}
                        </div>
                        @enderror
                    </div>
                    <div>
                        <input type="text" placeholder="Akun" id="nama_akun" name="nama_akun"
                            value="{{$sosmed->nama_akun}}" />
                        @error('nama_akun')
                        <div class="alert alert-danger">
                            {{message}}
                        </div>
                        @enderror
                    </div>
                    <div class="d-flex justify-content-center">
                        <button type="submit" class="btn_on-hover tombol">
                            Update
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

@endsection