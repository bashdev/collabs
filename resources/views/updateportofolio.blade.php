@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-6">
            <div class="detail-box">
                <div class="heading_container">
                    <h3>
                    <a href="{{url('/profil')}}">Profil</a>
                        <span>/Update Portofolio</span>
                    </h3>
                    <p>
                        Anda dapat mengubah portofolio anda disini
                    </p>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="contact-form">
                <h5>
                    Update Profil
                </h5>
                <form action="/portofolio/{{$portofolio->id}}" method="POST">
                    @csrf
                    @method('put')
                    <div>
                        <input type="text" placeholder="Nama Projek" id="nama_projek" name="nama_projek" value="{{$portofolio->nama_projek}}"/>
                        @error('nama_projek')
                        <div class="alert alert-danger">
                            {{message}}
                        </div>
                        @enderror
                    </div>
                    <div>
                        <input type="text" placeholder="Deskripsi" id="deskripsi" name="deskripsi" value="{{$portofolio->deskripsi}}"/>
                        @error('deskripsi')
                        <div class="alert alert-danger">
                            {{message}}
                        </div>
                        @enderror
                    </div>
                    <div class="d-flex justify-content-center">
                        <button type="submit" class="btn_on-hover tombol">
                            Update
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>


@endsection