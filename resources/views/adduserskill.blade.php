@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-6">
            <div class="detail-box">
                <div class="heading_container">
                    <h3>
                        <a href="{{url('/profil')}}">Profil</a>
                        <span>/+Skill</span>
                    </h3>
                    <p>
                        Anda dapat menambahkan kemampuan anda di bidang programming dan Sistem Informasi
                    </p>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="contact-form">
                <h5>
                    Tambah Skill
                </h5>
                <form action="/userskill" method="POST">
                    @csrf
                    <div class="form-group">
                        <label for="exampleFormControlSelect1" class="text-white">Skill</label>
                        <select class="form-control" id="skill_id" name="skill_id">
                            @foreach($skill as $value)
                            <option value="{{$value->id}}">{{$value->skill}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="d-flex justify-content-center">
                        <button type="submit" class="btn_on-hover tombol">
                            Tambah
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

@endsection