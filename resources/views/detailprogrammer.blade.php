@extends('layouts.app')
@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-6">
            <div class="detail-box mb-3">
                <div class="heading_container">
                    <h5 class="mb-4">
                        <a href="{{url('/')}}">Beranda</a>
                        <span>/Detail Profil</span>
                    </h5>
                    <img src="{{asset('/img/'.$profil->fotoprofil)}}" alt="fotoprofil" class="rounded-circle mb-4" height="200" width="200" />
                    <h2>Username</h2>
                    <p>{{$profil->user->name}}</p>
                    <h2>Bio</h2>
                    <p>
                        {!!$profil->bio!!}
                    </p>
                    <h2>Alamat</h2>
                    <p>{{$profil->alamat}}</p>
                    <h2>Telepon</h2>
                    <p>{{$profil->nohp}}</p>
                    <h2>Email</h2>
                    <p>{{$profil->user->email}}</p>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="container w-100 h-100 bg-secondary p-2 rounded">
                <h3>Daftar Kemampuan</h3>
                <hr class="bg-light">

                <table id="example" class="table table-light table-striped w-100">
                    <thead>
                        <tr>
                            <th style="width:10%">No</th>
                            <th>Skill</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($userskill as $key=>$value)
                        <tr>
                            <td>{{$key + 1}}</td>
                            <td>{{$value->skill->skill}}</td>
                        </tr>
                        @endforeach
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>No</th>
                            <th>Skill</th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4 my-3">
            <div class="bg-white border rounded pb-2" style="width:100%">
                <table class="w-100">
                    <thead>
                        <th class="pl-2">Sosmed</th>
                        <th>Akun</th>
                    </thead>
                    <tbody>
                        @foreach($sosmed as $value)
                        <tr>
                            <td class="pl-2" style="word-break:break-all;">
                                {{$value->sosmed}}
                            </td>
                            <td style="word-break:break-all;">
                                {{$value->nama_akun}}
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <div class="col-md-8 pb-5">
            <div class="container w-100 h-100 bg-secondary p-2 rounded">
                <h3>Daftar Portofolio</h3>
                <hr class="bg-light">
                <table id="example1" class="table table-light table-striped w-100">
                    <thead>
                        <tr>
                            <th style="width:10%">No</th>
                            <th style="width:25%">Nama Projek</th>
                            <th style="width:40%">Deskripsi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($portofolio as $key=>$value)
                        <tr>
                            <td>{{$key + 1}}</td>
                            <td>{{$value->nama_projek}}</td>
                            <td>{{$value->deskripsi}}</td>
                        </tr>
                        @endforeach
                    <tfoot>
                        <tr>
                            <th>No</th>
                            <th>Nama Projek</th>
                            <th>Deskripsi</th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</div>

@endsection
@push('scripts')
<script src="{{asset('./style/js/jquery.dataTables.js')}}"></script>
<script src="{{asset('./style/js/dataTables.bootstrap4.js')}}"></script>
<script>
$(function() {
    $("#example").DataTable({
        "bFilter": false,
        "lengthChange": false,
        "pageLength": 3
    });
    $("#example1").DataTable({
        "bFilter": false,
        "lengthChange": false,
        "pageLength": 3
    });
});
</script>
@endpush