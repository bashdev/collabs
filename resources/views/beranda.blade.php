@extends('layouts.app')
@section('content')


<main class="col-12 bg-light p-4">

    <div class="row mt-5 align-midle">
        @foreach($profil as $key=>$value)
        <div class="col-sm-4 mt-3">
            <div class="card bg-white p-2 shadow" style="text-align:center;">
                <img src="{{asset('./img/'.$value->fotoprofil)}}" class="card-img-top card-beranda rounded-circle" alt="avatar" height="200" width="200">
                <div class="card-body">
                    <h5 class="card-title">{{$value->user->name}}</h5>
                    <h6 class="card-title">{{$value->user->email}}</h6>
                    @foreach($userskill[$key] as $nilai)
                        <span class="bg-primary px-1 rounded text-white"><small>{{$nilai->skill->skill}}</small></span>
                    @endforeach
                    <div>
                    <a href="/beranda/{{$value->id}}" class="btn btn-success mt-3">Detail</a>
                    </div>
                </div>
            </div>
        </div>
        @endforeach
    </div>
    <!-- <div class="row mt-3 align-midle">
        <div class="col mt-2">
            <div class="card bg-white p-2 shadow" style="text-align:center;">
                <img src="{{asset('./img/user.png')}}" class="card-img-top card-beranda" alt="...">
                <div class="card-body">
                    <h5 class="card-title">Nama Programmer</h5>
                    <p class="card-text">bahasa Pemrograman yang dikuasai</p>
                    <a href="#" class="btn btn-primary">Detail</a>
                </div>
            </div>
        </div>
        <div class="col mt-3">
            <div class="card bg-white p-2 shadow" style="text-align:center;">
                <img src="{{asset('./img/user.png')}}" class="card-img-top card-beranda" alt="...">
                <div class="card-body">
                    <h5 class="card-title">Nama Programmer</h5>
                    <p class="card-text">bahasa Pemrograman yang dikuasai</p>
                    <a href="#" class="btn btn-primary">Detail</a>
                </div>
            </div>
        </div>
        <div class="col mt-3">
            <div class="card bg-white p-2 shadow" style="text-align:center;">
                <img src="{{asset('./img/user.png')}}" class="card-img-top card-beranda" alt="...">
                <div class="card-body">
                    <h5 class="card-title">Nama Programmer</h5>
                    <p class="card-text">bahasa Pemrograman yang dikuasai</p>
                    <a href="#" class="btn btn-primary">Detail</a>
                </div>
            </div>
        </div>
    </div> -->

</main>



@endsection