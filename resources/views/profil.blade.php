@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-6">
            <div class="detail-box mb-3">
                <div class="heading_container">
                    <h3>
                        Profil
                    </h3>
                    <img src="{{asset('./img/'.$profil->fotoprofil)}}" alt="fotoprofil" class="rounded-circle mb-4"
                        height="200" width="200">
                    <h2>Username</h2>
                    <p>{{$profil->user->name}}</p>
                    <h2>Bio</h2>
                    <p>
                        {!!$profil->bio!!}
                    </p>
                    <h2>Alamat</h2>
                    <p>{{$profil->alamat}}</p>
                    <h2>Telepon</h2>
                    <p>{{$profil->nohp}}</p>
                    <h2>Email</h2>
                    <p>{{$profil->user->email}}</p>
                    <a href="/profil/{{$profil->id}}/edit" class="btn btn-warning">update profile</a>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="container w-100 h-100 bg-secondary p-2 rounded">
                <h3>Daftar Kemampuan</h3>
                <hr class="bg-light">
                <div class="mb-2 d-flex justify-content-end">
                    <a href="{{url('/userskill/create')}}" class="btn btn-success border-light">+ skill</a>
                </div>
                <table id="example" class="table table-light table-striped w-100">
                    <thead>
                        <tr>
                            <th style="width:10%">No</th>
                            <th style="width:50%">Skill</th>
                            <th style="width:40%">Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($userskill as $key=>$value)
                        <tr>
                            <td>{{$key + 1}}</td>
                            <td>{{$value->skill->skill}}</td>
                            <td>
                                <a href="/userskill/{{$value->id}}/edit" class="btn btn-warning">edit</a>
                                <form action="/userskill/{{$value->id}}" method="POST" style="display:inline">
                                    @csrf
                                    @method('DELETE')
                                    <input type="submit" class="btn btn-danger my-1" value="hapus">
                                </form>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>No</th>
                            <th>Skill</th>
                            <th>Aksi</th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4 my-3">
            <div class="bg-white border rounded pb-2" style="width:100%">
                <div class="d-flex justify-content-end">
                    <a href="{{url('/sosmed/create')}}" class="btn-sm btn-success"><b>+ sosmed</b></a>
                </div>
                <table class="w-100">
                    <thead>
                        <th class="pl-2">Sosmed</th>
                        <th>Akun</th>
                        <th style="width:30%">aksi</th>
                    </thead>
                    <tbody>
                        @foreach($sosmed as $value)
                        <tr>
                            <td style="word-break:break-all;" class="pl-2">
                                {{$value->sosmed}}
                            </td>
                            <td style="word-break:break-all;">
                                {{$value->nama_akun}}
                            </td>
                            <td class="pt-2">
                                <a href="/sosmed/{{$value->id}}/edit" class="btn-sm btn-warning"><small>edit</small></a>
                                <form action="/sosmed/{{$value->id}}" method="POST" style="display:inline">
                                    @csrf
                                    @method('DELETE')
                                    <input type="submit" class="btn btn-sm btn-danger" value="hapus"
                                        style="font-size:12px">
                                </form>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <div class="col-md-8 pb-5">
            <div class="container w-100 h-100 bg-secondary p-2 rounded">
                <h3>Daftar Portofolio</h3>
                <hr class="bg-light">
                <div class="mb-2 d-flex justify-content-end">
                    <a href="{{url('/portofolio/create')}}" class="btn btn-success border-light">+ portofolio</a>
                </div>
                <div class="overflow-auto">
                    <table id="example1" class="table table-light table-striped">
                        <thead>
                            <tr>
                                <th style="width:10%">No</th>
                                <th style="width:25%">Nama Projek</th>
                                <th style="width:40%">Deskripsi</th>
                                <th style="width:25%">Aksi</th>
                            </tr>
                        </thead>

                        <tbody class="overflow-auto">
                            @foreach($portofolio as $key=>$value)
                            <tr>
                                <td>{{$key + 1}}</td>
                                <td>{{$value->nama_projek}}</td>
                                <td>{{$value->deskripsi}}</td>
                                <td>
                                    <a href="/portofolio/{{$value->id}}/edit" class="btn btn-warning">edit</a>
                                    <form action="/portofolio/{{$value->id}}" method="POST" style="display:inline">
                                        @csrf
                                        @method('DELETE')
                                        <input type="submit" class="btn btn-danger" value="hapus">
                                    </form>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>

                        <tfoot>
                            <tr>
                                <th>No</th>
                                <th>Nama Projek</th>
                                <th>Deskripsi</th>
                                <th>Aksi</th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
@push('scripts')
<script src="{{asset('./style/js/jquery.dataTables.js')}}"></script>
<script src="{{asset('./style/js/dataTables.bootstrap4.js')}}"></script>
<script>
$(function() {
    $("#example").DataTable({
        "bFilter": false,
        "lengthChange": false,
        "pageLength": 3
    });
    $("#example1").DataTable({
        "bFilter": false,
        "lengthChange": false,
        "pageLength": 3
    });
});
</script>
@endpush