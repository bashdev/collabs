@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-6">
            <div class="detail-box">
                <div class="heading_container">
                    <h3>
                        <a href="{{url('/profil')}}">Profil</a>
                        <span>/Update Profil</span>
                    </h3>
                    <p>
                        Anda dapat mengubah profil anda disini
                    </p>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="contact-form">
                <h5>
                    Update Profil
                </h5>
                <form action="/profil/{{$profil->id}}" method="POST" enctype="multipart/form-data">
                    @csrf
                    @method('put')
                    <div>
                        <input name="email" id="email" type="email" placeholder="Email" disabled="disabled"
                            value="{{$profil->user->email}}" />
                    </div>
                    <div>
                        <input name="name" id="name" type="text" placeholder="Username"
                            value="{{$profil->user->name}}" />
                        @error('name')
                        <div class="alert alert-danger">
                            {{message}}
                        </div>
                        @enderror
                    </div>
                    <div>
                        <input name="nohp" id="nohp" type="tel" placeholder="Telepon" value="{{$profil->nohp}}" />
                        @error('nohp')
                        <div class="alert alert-danger">
                            {{message}}
                        </div>
                        @enderror
                    </div>
                    <div>
                        <textarea name="bio" id="bio" cols="35" rows="5" placeholder="Bio"
                            class="rounded mt-2 mb-4">{{$profil->bio}}</textarea>
                        @error('bio')
                        <div class="alert alert-danger">
                            {{message}}
                        </div>
                        @enderror
                    </div>
                    <div>
                        <input name="alamat" id="alamat" type="text" placeholder="Alamat" value="{{$profil->alamat}}" />
                        @error('alamat')
                        <div class="alert alert-danger">
                            {{message}}
                        </div>
                        @enderror
                    </div>
                    <div>
                        <input id="fotoprofil" type="file" name="fotoprofil">
                        @error('fotoprofil')
                        <div class="alert alert-danger">
                            {{message}}
                        </div>
                        @enderror
                    </div>
                    <!-- <div>
                        <input type="text" placeholder="Message" class="input_message"/>
                    </div> -->
                    <div class="d-flex justify-content-center">
                        <button type="submit" class="tombol">
                            update
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@push('scripts')
<script src="https://cdn.tiny.cloud/1/e2g8957c442r93zx5muchbwhzra20ocv3s2bnf8d630qzits/tinymce/5/tinymce.min.js"
    referrerpolicy="origin"></script>
<script>
tinymce.init({
    selector: 'textarea',
    plugins: 'a11ychecker advcode casechange export formatpainter linkchecker autolink lists checklist media mediaembed pageembed permanentpen powerpaste table advtable tinycomments tinymcespellchecker',
    toolbar: 'a11ycheck addcomment showcomments casechange checklist code export formatpainter pageembed permanentpen table',
    toolbar_mode: 'floating',
    tinycomments_mode: 'embedded',
    tinycomments_author: 'Author name',
});
</script>
@endpush
@endsection