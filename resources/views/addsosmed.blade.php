@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-6">
            <div class="detail-box">
                <div class="heading_container">
                    <h3>
                        <a href="{{url('/profil')}}">Profil</a>
                        <span>/+Sosmed</span>
                    </h3>
                    <p>
                        Anda dapat menambahkan sosial media ataupun website atau blog portofolio anda
                    </p>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="contact-form">
                <h5>
                    Tambah Sosial Media
                </h5>
                <form action="/sosmed" method="POST">
                    @csrf
                    <div>
                        <input type="text" placeholder="Sosial Media" id="sosmed" name="sosmed" />
                        @error('sosmed')
                        <div class="alert alert-danger">
                            {{message}}
                        </div>
                        @enderror
                    </div>
                    <div>
                        <input type="text" placeholder="Akun" id="nama_akun" name="nama_akun" />
                        @error('nama_akun')
                        <div class="alert alert-danger">
                            {{message}}
                        </div>
                        @enderror
                    </div>
                    <div class="d-flex justify-content-center">
                        <button type="submit" class="btn_on-hover tombol">
                            Tambah
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

@endsection