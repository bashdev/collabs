## ERD 

<p align="center">
<img src="./public/img/ERD_FIX.png" alt="Build Status">
</p> <br>

<pre>
Kelompok 16
Ahmad Mustamid Abas (@shiriyu) ahmad.abas@students.amikom.ac.id
Tatag Hardoyo (@tataghrdy) tataghardoyo@gmail.com
Vincentius Bagus Hermawan Putra (@Yoshh) vincentresek@gmail.com

Tema Web yang dipilih : Catalog Programmer
Isi Web : 

- Melihat daftar Programmer
- Melihat Bahasa yg dikuasai Programmer
- Melihat Melihat Portofolio programmer
- Mendaftar Jadi User di Web Catalog Programmer
- Menambahkan Bahasa yang dikuasai
- Menambahkan Portofolio

link deploy :
<a href="http://catalogprgmr.herokuapp.com/">Link deploy Heroku</a>
link demo program :
<a href="https://youtu.be/ohBVwFzHXqA">Link Demo Program</a>
</pre>




