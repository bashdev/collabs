<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Sosmed;
use Auth;
use RealRashid\SweetAlert\Facades\Alert;
class SosmedController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $sosmed = Sosmed::where('user_id', Auth::user()->id);
        // return view('profil', compact('sosmed'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('addsosmed');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
    		'sosmed' => 'required',
            'nama_akun' => 'required',
    	]);
 
        Sosmed::create([
    		'user_id' => Auth::user()->id,
    		'sosmed' => $request->sosmed,
            'nama_akun' => $request->nama_akun
    	]);
        Alert::success('Berhasil', 'Berhasil menambah skill');
    	return redirect('/profil');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $sosmed = Sosmed::find($id);
        return view('updatesosmed', compact('sosmed'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
    		'sosmed' => 'required',
            'nama_akun' => 'required',
    	]);

        $sosmed = Sosmed::find($id);
        $sosmed->sosmed = $request->sosmed;
        $sosmed->nama_akun = $request->nama_akun;
        $sosmed->update();
        Alert::success('Berhasil', 'Berhasil mengupdate skill');
        return redirect('/profil');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $sosmed = Sosmed::find($id);
        $sosmed->delete();
        Alert::success('Berhasil', 'Berhasil menghapus skill');
        return redirect('/profil');
    }
}
