<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Profil;
use App\User;
use App\Sosmed;
use App\Portofolio;
use App\Userskill;
use Auth;
use Storage;
use RealRashid\SweetAlert\Facades\Alert;
class ProfilController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $userskill = Userskill::where('user_id', Auth::user()->id)->get();
        $portofolio = Portofolio::where('user_id', Auth::user()->id)->get();
        $sosmed = Sosmed::where('user_id', Auth::user()->id)->get();
        $profil=Profil::where('user_id', Auth::user()->id)->first();
        return view('profil', compact('profil','sosmed', 'portofolio','userskill'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $profil = Profil::find($id);
        return view('updateprofil', compact('profil'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'name' => 'required',
            'nohp' => 'required',
            'bio' => 'required',
            'alamat' => 'required',
            'fotoprofil' => 'mimes:jpeg,jpg,png'
        ]);

        $profil = Profil::findorfail($id);
        $user = User::findorfail($id);

        if($request->has('fotoprofil')){
            $path = '/home/tatag/Documents/last/collabs/public/img/'.$profil->fotoprofil;
            //unlink($path);
            $gambar = $request->fotoprofil;
            $new_gambar = time(). ' - '.$gambar->getClientOriginalName();
            $gambar->move('img/', $new_gambar);
            $editdata = [
                'name' => $request->name
            ];
            $editdata2 = [
                'nohp' => $request->nohp,
                'bio' => $request->bio,
                'alamat' => $request->alamat,
                'fotoprofil' => $new_gambar
            ];
        }else{
            $editdata = [
                'name' => $request->name
            ];
            $editdata2 = [
                'nohp' => $request->nohp,
                'bio' => $request->bio,
                'alamat' => $request->alamat,
            ];
        }
        $profil->update($editdata2);
        $user->update($editdata);
        Alert::success('Berhasil', 'Berhasil mengupdate profil');
        return redirect('/profil');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
