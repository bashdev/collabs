<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Skill;
use App\Userskill;
use Auth;
use RealRashid\SweetAlert\Facades\Alert;
class UserskilController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $skill = Skill::all();
        return view('adduserskill',compact('skill'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
    		'skill_id' => 'required',
    	]);
 
        Userskill::create([
    		'user_id' => Auth::user()->id,
    		'skill_id' => $request->skill_id
    	]);
        Alert::success('Berhasil', 'Berhasil menambah skill');
    	return redirect('/profil');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $skill = Skill::all();
        $userskill = Userskill::find($id);
        return view('updateuserskill', compact('userskill','skill'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
    		'skill_id' => 'required',
    	]);

        $userskill = Userskill::find($id);
        $userskill->user_id = Auth::user()->id;
        $userskill->skill_id = $request->skill_id;
        $userskill->update();
        Alert::success('Berhasil', 'Berhasil mengupdate skill');
        return redirect('/profil');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $userskill = Userskill::find($id);
        $userskill->delete();
        Alert::success('Berhasil', 'Berhasil menghapus skill');
        return redirect('/profil');
    }
}
