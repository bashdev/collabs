<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Userskill;
use App\Profil;
use App\Portofolio;
use App\Sosmed;
class BerandaController extends Controller
{
    public function index(){
        $profil=Profil::all();
        $profile=Profil::all()->count();
        for($i=0; $i<$profile; $i++){
            $userskill[$i] = Userskill::where('user_id', $i+1)->limit(3)->get();
        }

        return view('beranda',compact('profil','userskill','profile'));
    }
    public function show($user_id){
        $userskill = Userskill::where('user_id', $user_id)->get();
        $portofolio = Portofolio::where('user_id', $user_id)->get();
        $sosmed = Sosmed::where('user_id', $user_id)->get();
        $profil = Profil::where('user_id', $user_id)->first();
        return view('detailprogrammer',compact('userskill','portofolio','sosmed','profil'));
    }
}
