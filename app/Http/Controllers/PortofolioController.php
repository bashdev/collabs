<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Portofolio;
use RealRashid\SweetAlert\Facades\Alert;
class PortofolioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('addportofolio');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
    		'nama_projek' => 'required',
            'deskripsi' => 'required',
    	]);
 
        Portofolio::create([
    		'user_id' => Auth::user()->id,
    		'nama_projek' => $request->nama_projek,
            'deskripsi' => $request->deskripsi
    	]);
        Alert::success('Berhasil', 'Berhasil menambah portofolio');
    	return redirect('/profil');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $portofolio = Portofolio::find($id);
        return view('updateportofolio', compact('portofolio'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
    		'nama_projek' => 'required',
            'deskripsi' => 'required',
    	]);

        $portofolio = Portofolio::find($id);
        $portofolio->nama_projek = $request->nama_projek;
        $portofolio->deskripsi = $request->deskripsi;
        $portofolio->update();
        Alert::success('Berhasil', 'Berhasil mengupdate portofolio');
        return redirect('/profil');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $portofolio = Portofolio::find($id);
        $portofolio->delete();
        Alert::success('Berhasil', 'Berhasil menghapus portofolio');
        return redirect('/profil');
    }
}
