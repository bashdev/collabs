<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sosmed extends Model
{
    protected $table = 'sosmed';
    protected $fillable = ['user_id', 'sosmed', 'nama_akun'];
    public $timestamps = false;
    public function user(){
        return $this->belongsTo('App\User');
    }
}
