<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Portofolio extends Model
{
    protected $table = 'portofolio';
    protected $fillable = ['user_id', 'nama_projek','deskripsi'];
    public $timestamps = false;
    public function user(){
        return $this->belongsTo('App\User');
    }
}
