<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Skill extends Model
{
    protected $table = 'skill';
    protected $fillable = ['skill'];
    public $timestamps = false;
    public function userskill(){
        return $this->hasMany('App\Userskill');
    }
}
