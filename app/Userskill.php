<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Userskill extends Model
{
    protected $table = 'userskill';
    protected $fillable = ['skill_id', 'user_id'];
    public $timestamps = false;
    
    public function user()
    {
     return $this->belongsTo('App\User');
    }  
    public function skill()
    {
     return $this->belongsTo('App\Skill');
    }
}
