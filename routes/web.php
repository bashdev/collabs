<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','BerandaController@index');
Route::get('/beranda/{user_id}','BerandaController@show');

Route::group(['middleware' => ['auth']], function(){
    Route::resource('profil', 'ProfilController')->only([
        'index','edit','update'
    ]);
    Route::resource('userskill', 'UserskilController')->only([
        'create','store','edit','update','destroy'
    ]);
    Route::resource('sosmed', 'SosmedController')->only([
        'create','store','edit','update','destroy'
    ]);
    Route::resource('portofolio', 'PortofolioController')->only([
        'create', 'store','edit','update','destroy'
    ]);
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
